Source: lomiri-content-hub
Priority: optional
Maintainer: UBports Developers <developers@ubports.com>
Build-Depends: click-dev,
               cmake,
               cmake-extras,
               dbus-test-runner <!nocheck>,
               debhelper-compat (= 12),
               dh-apparmor,
               dh-migrations | hello,
               doxygen <!nodoc>,
               google-mock <!nocheck>,
               graphviz <!nodoc>,
               libgtest-dev,
               lcov <!nocheck>,
               libapparmor-dev,
               libdbus-1-dev,
               libglib2.0-dev,
               libgsettings-qt-dev,
               liblibertine-dev <!pkg.lomiri-content-hub.bootstrap>,
               liblomiri-api-dev,
               libnotify-dev,
               liblomiri-download-manager-client-dev,
               liblomiri-app-launch-dev,
               qml-module-lomiri-components,
               qml-module-qtquick2,
               qml-module-qttest <!nocheck>,
               qtbase5-dev,
               qtdeclarative5-dev,
               qtdeclarative5-dev-tools,
               qttools5-dev-tools,
               xauth <!nocheck>,
               xvfb <!nocheck>,
Standards-Version: 3.9.4
Section: libs
Homepage: https://gitlab.com/ubports/development/core/lomiri-content-hub
Vcs-Git: https://gitlab.com/ubports/development/core/lomiri-content-hub.git
Vcs-Browser: https://gitlab.com/ubports/development/core/lomiri-content-hub
X-Ubuntu-Use-Langpack: yes

Package: lomiri-content-hub
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Replaces: content-hub (<< 2.0.0)
Breaks: content-hub (<< 2.0.0)
Provides: content-hub (=${binary:Version})
Description: content sharing/picking service
 Content sharing/picking infrastructure and service, designed to allow apps to
 securely and efficiently exchange content.
 This package includes the content sharing service.

Package: content-hub
Depends: lomiri-content-hub, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: liblomiri-content-hub1
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
Suggests: lomiri-content-hub
Description: content sharing/picking library
 Content sharing/picking infrastructure and service, designed to allow apps to
 securely and efficiently exchange content.
 This package includes the content sharing libraries.

Package: liblomiri-content-hub-glib1
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
         ${shlibs:Depends},
Suggests: lomiri-content-hub
Description: content sharing/picking library - GLib bindings
 Content sharing/picking infrastructure and service, designed to allow apps to
 securely and efficiently exchange content.
 .
 This package includes GLib bindings of the content sharing libraries.

Package: liblomiri-content-hub-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: liblomiri-content-hub1 (= ${binary:Version}),
         ${misc:Depends},
Description: content sharing development files
 All the development headers and libraries for the content hub

Package: liblomiri-content-hub-glib-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: liblomiri-content-hub-glib1 (= ${binary:Version}),
         ${misc:Depends},
Description: content sharing development files - GLib bindings
 GLib bindings of the development headers and libraries for the content hub

Package: qml-module-lomiri-content
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends},
Depends: qml-module-qtquick2,
         ${misc:Depends},
         ${shlibs:Depends},
Description: QML binding for liblomiri-content-hub
 QML bindings for liblomiri-content-hub

Package: qml-module-ubuntu-content
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends},
Depends: qml-module-lomiri-content,
         qml-module-qtquick2,
         ${misc:Depends},
         ${shlibs:Depends},
Provides: qtdeclarative5-ubuntu-content0.1,
          qtdeclarative5-ubuntu-content1,
Breaks: qtdeclarative5-ubuntu-content1,
Replaces: qtdeclarative5-ubuntu-content1,
Description: QML binding for liblomiri-content-hub - Ubuntu.Content compatibility layer
 This package contains the wrapper around Lomiri.Content QML type to provide
 Ubuntu.Content QML type, for compatibility with older applications.

Package: liblomiri-content-hub-doc
Section: doc
Architecture: all
Build-Profiles: <!nodoc>
Depends: liblomiri-content-hub-dev (>= ${source:Version}),
         ${misc:Depends},
Description: Documentation files for liblomiri-content-hub-dev
 Documentation files for the liblomiri-content-hub development

Package: lomiri-content-hub-testability
Section: libdevel
Architecture: any
Build-Profiles: <!noinsttest>
Depends: ${misc:Depends},
         ${shlibs:Depends},
         lomiri-content-hub,
Replaces: content-hub-testability (<< 2.0.0)
Breaks: content-hub-testability (<< 2.0.0)
Provides: content-hub-testability (=${binary:Version})
Description: content sharing testability
 Files and utilities needed for automated testing of lomiri-content-hub

Package: content-hub-testability
Depends: lomiri-content-hub-testability, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.
