# add a target to generate API documentation with qdoc
SET(QDOC_WORKS FALSE)
SET(ENV{QT_SELECT} qt5)
FIND_PROGRAM(QDOC_EXECUTABLE qdoc)
if(QDOC_EXECUTABLE)
EXECUTE_PROCESS(COMMAND ${QDOC_EXECUTABLE} --version OUTPUT_VARIABLE QDOC_OUTPUT ERROR_QUIET)
string(REGEX REPLACE "qdoc ([0-9]+(\\.[0-9]+)+).*" "\\1" QDOC_VERSION ${QDOC_OUTPUT})
if(QDOC_VERSION MATCHES "^5\\.")
set(QDOC_WORKS TRUE)
endif()
endif(QDOC_EXECUTABLE)
if(QDOC_WORKS)
message(STATUS "Check for working qdoc: ${QDOC_EXECUTABLE} (version: \"${QDOC_VERSION}\") -- works")
elseif(QDOC_EXECUTABLE)
message(FATAL_ERROR "Check for working qdoc: ${QDOC_EXECUTABLE} -- does not work")
else()
message(FATAL_ERROR "Check for working qdoc: not found")
endif(QDOC_WORKS)

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/lomiri-content-hub.qdocconf.in ${CMAKE_CURRENT_BINARY_DIR}/lomiri-content-hub.qdocconf @ONLY)
add_custom_target(qmldoc
${QDOC_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/lomiri-content-hub.qdocconf
WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
COMMENT "Generating QML API documentation with qdoc" VERBATIM
)

# copy stylesheet files into build directory for shadow builds
file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/css"
    DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
    )

install(
  DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/html/
  DESTINATION ${CMAKE_INSTALL_DOCDIR}/qml/html
)

add_dependencies(doc qmldoc)
