/*
 * Copyright 2013 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COM_LOMIRI_CONTENTTRANSFER_H_
#define COM_LOMIRI_CONTENTTRANSFER_H_

#include "contentstore.h"
#include "contenttype.h"

#include <com/lomiri/content/store.h>
#include <com/lomiri/content/transfer.h>

#include <QList>
#include <QObject>
#include <QQmlListProperty>

class ContentItem;

class ContentTransfer : public QObject
{
    Q_OBJECT
    Q_ENUMS(State)
    Q_ENUMS(Direction)
    Q_ENUMS(SelectionType)
    Q_PROPERTY(State state READ state WRITE setState NOTIFY stateChanged)
    Q_PROPERTY(Direction direction READ direction CONSTANT)
    Q_PROPERTY(SelectionType selectionType READ selectionType WRITE setSelectionType NOTIFY selectionTypeChanged)
    Q_PROPERTY(QString store READ store NOTIFY storeChanged)
    Q_PROPERTY(QQmlListProperty<ContentItem> items READ items NOTIFY itemsChanged)
    Q_PROPERTY(QString downloadId READ downloadId WRITE setDownloadId NOTIFY downloadIdChanged)
    Q_PROPERTY(ContentType::Type contentType READ contentType CONSTANT)
    Q_PROPERTY(QString source READ source)
    Q_PROPERTY(QString destination READ destination)

public:
    enum State {
        Created = com::lomiri::content::Transfer::created,
        Initiated = com::lomiri::content::Transfer::initiated,
        InProgress = com::lomiri::content::Transfer::in_progress,
        Charged = com::lomiri::content::Transfer::charged,
        Collected = com::lomiri::content::Transfer::collected,
        Aborted = com::lomiri::content::Transfer::aborted,
        Finalized = com::lomiri::content::Transfer::finalized,
        Downloading = com::lomiri::content::Transfer::downloading,
        Downloaded = com::lomiri::content::Transfer::downloaded
    };
    enum Direction {
        Import = com::lomiri::content::Transfer::Import,
        Export = com::lomiri::content::Transfer::Export,
        Share = com::lomiri::content::Transfer::Share
    };
    enum SelectionType {
        Single = com::lomiri::content::Transfer::SelectionType::single,
        Multiple = com::lomiri::content::Transfer::SelectionType::multiple
    };

    ContentTransfer(QObject *parent = nullptr);

    State state() const;
    void setState(State state);

    Direction direction() const;

    SelectionType selectionType() const;
    void setSelectionType(SelectionType);

    QQmlListProperty<ContentItem> items();

    Q_INVOKABLE bool start();
    Q_INVOKABLE bool finalize();

    const QString store() const;
    Q_INVOKABLE void setStore(ContentStore *contentStore);

    com::lomiri::content::Transfer *transfer() const;
    void setTransfer(com::lomiri::content::Transfer *transfer);

    QString downloadId();
    void setDownloadId(QString downloadId);

    void collectItems();

    ContentType::Type contentType() const;

    QString source();
    QString destination();

Q_SIGNALS:
    void stateChanged();
    void itemsChanged();
    void selectionTypeChanged();
    void storeChanged();
    void downloadIdChanged();

private Q_SLOTS:
    void updateState();
    void updateSelectionType();

private:
    com::lomiri::content::Transfer *m_transfer;
    QList<ContentItem *> m_items;
    State m_state;
    Direction m_direction;
    SelectionType m_selectionType;
    ContentStore * m_contentStore;
};

#endif // COM_LOMIRI_CONTENTTRANSFER_H_
