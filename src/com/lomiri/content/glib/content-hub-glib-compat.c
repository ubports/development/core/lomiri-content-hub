/*
 * Copyright © 2023 UBports Foundation.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "glib/content-hub-glib.h"

void content_hub_transfer_call_store (
    ContentHubTransfer *proxy,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data)
{
    content_hub_transfer_call_store_with_scope_and_type(
        proxy, cancellable, callback, user_data);
}

gboolean content_hub_transfer_call_store_finish (
    ContentHubTransfer *proxy,
    gchar **out_uri,
    GAsyncResult *res,
    GError **error)
{
    return content_hub_transfer_call_store_with_scope_and_type_finish(
        proxy, /* out_scope */ NULL, /* out_type_id */ NULL, out_uri,
        res, error);
}

gboolean content_hub_transfer_call_store_sync (
    ContentHubTransfer *proxy,
    gchar **out_uri,
    GCancellable *cancellable,
    GError **error)
{
    return content_hub_transfer_call_store_with_scope_and_type_sync(
        proxy, /* out_scope */ NULL, /* out_type_id */ NULL, out_uri,
        cancellable, error);
}

void content_hub_transfer_call_set_store (
    ContentHubTransfer *proxy,
    const gchar *arg_uri,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data)
{
    g_warning ("Setting store by path directly is no longer supported. "
               "To avoid surprise, this transfer will be aborted.");

    content_hub_transfer_call_abort(
        proxy, cancellable, callback, user_data);
}

gboolean content_hub_transfer_call_set_store_finish (
    ContentHubTransfer *proxy,
    GAsyncResult *res,
    GError **error)
{
    if (!content_hub_transfer_call_abort_finish (
            proxy, res, /* error */ NULL)) {
        g_warning ("The abort call fails. The further state of transfer is unknown.");
    }

    /* Whether the abort fails or not, return FALSE for set_store operation. */
    g_dbus_error_set_dbus_error(
        error,
        "org.freedesktop.DBus.Error.NotSupported",
        "Setting store by path directly is no longer supported.",
        NULL);

    return FALSE;
}

gboolean content_hub_transfer_call_set_store_sync (
    ContentHubTransfer *proxy,
    const gchar *arg_uri,
    GCancellable *cancellable,
    GError **error)
{
    g_warning ("Setting store by path directly is no longer supported. "
               "To avoid surprise, this transfer will be aborted.");

    if (!content_hub_transfer_call_abort_sync (
            proxy, cancellable, /* error */ NULL)) {
        g_warning ("The abort call fails. The further state of transfer is unknown.");
    }

    /* Whether the abort fails or not, return FALSE for set_store operation. */
    g_dbus_error_set_dbus_error(
        error,
        "org.freedesktop.DBus.Error.NotSupported",
        "Setting store by path directly is no longer supported.",
        NULL);

    return FALSE;
}

void
content_hub_transfer_complete_set_store (
    ContentHubTransfer *object,
    GDBusMethodInvocation *invocation)
{
  g_dbus_method_invocation_return_value (invocation,
    g_variant_new ("()"));
}

void
content_hub_transfer_complete_store (
    ContentHubTransfer *object,
    GDBusMethodInvocation *invocation,
    const gchar *uri)
{
  g_dbus_method_invocation_return_value (invocation,
    g_variant_new ("(s)",
                   uri));
}
