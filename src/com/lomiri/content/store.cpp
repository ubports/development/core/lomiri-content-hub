/*
 * Copyright © 2013 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Thomas Voß <thomas.voss@canonical.com>
 */

#include <com/lomiri/content/store.h>

#include <QDebug>

#include <com/lomiri/content/scope.h>
#include <com/lomiri/content/type.h>

namespace cuc = com::lomiri::content;

struct cuc::Store::Private
{
    cuc::Scope scope;
    cuc::Type type;
    QString uri;
};

cuc::Store::Store(const QString& uri, QObject* parent)
    : QObject(parent),
    d(new cuc::Store::Private{Scope::unknown, Type::unknown(), uri})
{
    qWarning() << "Directly specifying store path is no longer supported "
                  "and will result in your transfer being aborted. "
                  "Please fix the application.";
}

cuc::Store::Store(Scope scope, const Type & type, const QString& uri, QObject* parent)
    : QObject(parent),
    d(new cuc::Store::Private{scope, type, uri})
{
}

cuc::Store::Store(const cuc::Store& rhs) : QObject(rhs.parent()), d(rhs.d)
{
}

cuc::Store& cuc::Store::operator=(const cuc::Store& rhs)
{
    d = rhs.d;
    return *this;
}

cuc::Store::~Store()
{
}

cuc::Scope cuc::Store::scope() const
{
    return d->scope;
}

const cuc::Type& cuc::Store::type() const
{
    return d->type;
}

const QString& cuc::Store::uri() const
{
    return d->uri;
}

