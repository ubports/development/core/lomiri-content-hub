# Vietnamese translation for content-hub
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the content-hub package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: content-hub\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-04 07:08+0000\n"
"PO-Revision-Date: 2015-01-26 03:08+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Vietnamese <vi@li.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-12-01 04:56+0000\n"
"X-Generator: Launchpad (build 18282)\n"

#: src/com/lomiri/content/detail/service.cpp:245
msgid "Download Complete"
msgstr ""

#: src/com/lomiri/content/detail/service.cpp:263
msgid "Open"
msgstr ""

#: src/com/lomiri/content/detail/service.cpp:270
msgid "Dismiss"
msgstr ""

#: src/com/lomiri/content/detail/service.cpp:288
msgid "Download Failed"
msgstr ""

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Choose from"
msgstr ""

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Open with"
msgstr ""

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Share to"
msgstr ""

#: import/Lomiri/Content/ContentPeerPicker10.qml:162
#: import/Lomiri/Content/ContentPeerPicker13.qml:170
#: import/Lomiri/Content/ContentPeerPicker11.qml:180
msgid "Apps"
msgstr ""

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can provide this type "
"of content."
msgstr ""

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can handle this type "
"of content."
msgstr ""

#: import/Lomiri/Content/ContentPeerPicker10.qml:214
#: import/Lomiri/Content/ContentPeerPicker13.qml:223
#: import/Lomiri/Content/ContentPeerPicker11.qml:232
msgid "Devices"
msgstr ""

#: import/Lomiri/Content/ContentPeerPicker10.qml:251
#: import/Lomiri/Content/ContentPeerPicker11.qml:59
#: import/Lomiri/Content/ContentTransferHint.qml:65
msgid "Cancel"
msgstr ""

#: import/Lomiri/Content/ContentTransferHint.qml:52
msgid "Transfer in progress"
msgstr ""

#: examples/picker-qml/picker.qml:21
msgid "Peer Picker Example"
msgstr ""

#: examples/picker-qml/picker.qml:37
msgid "Sources"
msgstr ""

#: examples/picker-qml/picker.qml:39
msgid "Select source"
msgstr ""

#: examples/picker-qml/picker.qml:52
msgid "Results"
msgstr ""
