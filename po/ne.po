# Nepali translation for content-hub
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the content-hub package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: content-hub\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-04 07:08+0000\n"
"PO-Revision-Date: 2023-01-04 17:00+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Nepali <https://hosted.weblate.org/projects/lomiri/content-"
"hub/ne/>\n"
"Language: ne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2016-12-01 04:56+0000\n"

#: src/com/lomiri/content/detail/service.cpp:245
msgid "Download Complete"
msgstr "डाउनलोड सम्पन्न"

#: src/com/lomiri/content/detail/service.cpp:263
msgid "Open"
msgstr "खुला"

#: src/com/lomiri/content/detail/service.cpp:270
msgid "Dismiss"
msgstr "खारेज"

#: src/com/lomiri/content/detail/service.cpp:288
msgid "Download Failed"
msgstr "डाउनलोड असफल"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Choose from"
msgstr "निम्नबाट छान्नुहोस्"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Open with"
msgstr "निम्नबाट खोल्नुहोस्"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Share to"
msgstr "निम्नसंग बाँड्नुहोस्"

#: import/Lomiri/Content/ContentPeerPicker10.qml:162
#: import/Lomiri/Content/ContentPeerPicker13.qml:170
#: import/Lomiri/Content/ContentPeerPicker11.qml:180
msgid "Apps"
msgstr "अनुप्रयोगहरू"

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can provide this type "
"of content."
msgstr ""
"माफ गर्नुहोस् , यस प्रकारको सामग्री प्रदान गर्न सक्ने कुनै पनि अनुप्रयोगहरू हाल स्थापित "
"भएका छैनन् ।"

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can handle this type "
"of content."
msgstr ""
"माफ गर्नुहोस् , यस प्रकारको सामग्रीलाई ह्यान्डल गर्न सक्ने कुनै पनि अनुप्रयोगहरू हाल स्थापित "
"भएका छैनन् ।"

#: import/Lomiri/Content/ContentPeerPicker10.qml:214
#: import/Lomiri/Content/ContentPeerPicker13.qml:223
#: import/Lomiri/Content/ContentPeerPicker11.qml:232
msgid "Devices"
msgstr "यन्त्रहरू"

#: import/Lomiri/Content/ContentPeerPicker10.qml:251
#: import/Lomiri/Content/ContentPeerPicker11.qml:59
#: import/Lomiri/Content/ContentTransferHint.qml:65
#, fuzzy
msgid "Cancel"
msgstr "Cancel"

#: import/Lomiri/Content/ContentTransferHint.qml:52
msgid "Transfer in progress"
msgstr "सार्ने कार्य हुँदैछ"

#: examples/picker-qml/picker.qml:21
msgid "Peer Picker Example"
msgstr ""

#: examples/picker-qml/picker.qml:37
msgid "Sources"
msgstr ""

#: examples/picker-qml/picker.qml:39
msgid "Select source"
msgstr ""

#: examples/picker-qml/picker.qml:52
msgid "Results"
msgstr ""
