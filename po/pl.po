# Polish translation for content-hub
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the content-hub package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: content-hub\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-04 07:08+0000\n"
"PO-Revision-Date: 2023-01-31 14:42+0000\n"
"Last-Translator: gnu-ewm <gnu.ewm@protonmail.com>\n"
"Language-Team: Polish <https://hosted.weblate.org/projects/lomiri/content-"
"hub/pl/>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.16-dev\n"
"X-Launchpad-Export-Date: 2016-12-01 04:56+0000\n"

#: src/com/lomiri/content/detail/service.cpp:245
msgid "Download Complete"
msgstr "Zakończono pobieranie"

#: src/com/lomiri/content/detail/service.cpp:263
msgid "Open"
msgstr "Otwórz"

#: src/com/lomiri/content/detail/service.cpp:270
msgid "Dismiss"
msgstr "Odrzuć"

#: src/com/lomiri/content/detail/service.cpp:288
msgid "Download Failed"
msgstr "Pobieranie nieudane"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Choose from"
msgstr "Wybierz z"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Open with"
msgstr "Otwórz za pomocą"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Share to"
msgstr "Udostępnij dla"

#: import/Lomiri/Content/ContentPeerPicker10.qml:162
#: import/Lomiri/Content/ContentPeerPicker13.qml:170
#: import/Lomiri/Content/ContentPeerPicker11.qml:180
msgid "Apps"
msgstr "Programy"

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can provide this type "
"of content."
msgstr ""
"Niestety nie zainstalowano żadnego programu, który oferowałby treści tego "
"typu."

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can handle this type "
"of content."
msgstr ""
"Niestety nie zainstalowano żadnego programu, który obsługiwałby treści tego "
"typu."

#: import/Lomiri/Content/ContentPeerPicker10.qml:214
#: import/Lomiri/Content/ContentPeerPicker13.qml:223
#: import/Lomiri/Content/ContentPeerPicker11.qml:232
msgid "Devices"
msgstr "Urządzenia"

#: import/Lomiri/Content/ContentPeerPicker10.qml:251
#: import/Lomiri/Content/ContentPeerPicker11.qml:59
#: import/Lomiri/Content/ContentTransferHint.qml:65
msgid "Cancel"
msgstr "Anuluj"

#: import/Lomiri/Content/ContentTransferHint.qml:52
msgid "Transfer in progress"
msgstr "Przesyłanie w toku"

#: examples/picker-qml/picker.qml:21
msgid "Peer Picker Example"
msgstr "Przykład selektora peer-to-peer"

#: examples/picker-qml/picker.qml:37
msgid "Sources"
msgstr "Źródła"

#: examples/picker-qml/picker.qml:39
msgid "Select source"
msgstr "Wybierz źródło"

#: examples/picker-qml/picker.qml:52
msgid "Results"
msgstr "Wyniki"
